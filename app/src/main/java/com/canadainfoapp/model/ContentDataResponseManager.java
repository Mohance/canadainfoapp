package com.canadainfoapp.model;

import com.canadainfoapp.pojos.ContentDataBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mohan on 28-11-2019.
 */

public class ContentDataResponseManager implements Callback<ContentDataBean> {
    ContentDataLoaderListener contentDataLoaderListener;
    ContentDataResponseInterface contentDataResponseInterface;

    public ContentDataResponseManager(ContentDataLoaderListener contentDataLoaderListener) {
        this.contentDataLoaderListener = contentDataLoaderListener;
        contentDataResponseInterface = ApiClient.getInstance().create(ContentDataResponseInterface.class);
    }

    //fetch data from url.
    public void getContentData() {
        Call<ContentDataBean> call = contentDataResponseInterface.getContentData();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<ContentDataBean> call, Response<ContentDataBean> response) {
        contentDataLoaderListener.onSuccess(response.body());
    }

    @Override
    public void onFailure(Call<ContentDataBean> call, Throwable t) {
        contentDataLoaderListener.onFailure(t.getLocalizedMessage());
    }
}

