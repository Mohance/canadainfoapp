package com.canadainfoapp.model;

import com.canadainfoapp.Utils;
import com.canadainfoapp.pojos.ContentDataBean;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Mohan on 28-11-2019.
 */

public interface ContentDataResponseInterface {
    @GET(Utils.CONTENT_DATA_BEAN)
    Call<ContentDataBean> getContentData();
}
