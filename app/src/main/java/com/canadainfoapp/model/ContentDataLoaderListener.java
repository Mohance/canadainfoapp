package com.canadainfoapp.model;

import com.canadainfoapp.pojos.ContentDataBean;

/**
 * Created by Mohan on 28-11-2019.
 */

public interface ContentDataLoaderListener {
    void onSuccess(ContentDataBean contentList);

    void onFailure(String error);
}
