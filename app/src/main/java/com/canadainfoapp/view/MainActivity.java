package com.canadainfoapp.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.canadainfoapp.R;
import com.canadainfoapp.Utils;
import com.canadainfoapp.adapter.ContentDataAdapter;
import com.canadainfoapp.pojos.ContentDataBean;
import com.canadainfoapp.pojos.RowsDataBean;
import com.canadainfoapp.presenter.ContentDataContract;
import com.canadainfoapp.presenter.ContentDataPresenter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ContentDataContract.ViewContract{
    private static String TAG = "MainActivity";
    ArrayList<RowsDataBean> rowsDataBeen;
    ProgressDialog progressDialog;
    private Button btRefresh;
    private RecyclerView mRecyclerView;
    private ContentDataAdapter contentDataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initViews();
        mDataFetchAndPopulate();
    }

    //init widgets
    private void initViews(){
        btRefresh = (Button) findViewById(R.id.bt_refresh);
        mRecyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        btRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDataFetchAndPopulate();
            }
        });
    }

    //Data fetch and populated from server.
    private void mDataFetchAndPopulate(){
        if(Utils.mChechConnectivity(this)) {
            ContentDataPresenter contentDataPresenter = new ContentDataPresenter(this);
            contentDataPresenter.getContentData();
        } else {
            Toast.makeText(this,getString(R.string.please_check_internet_connection_and_try_again),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void alertUser(String title, String text) {

    }

    @Override
    public void displayLoading(String title, String msg) {
        if(progressDialog==null){
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(msg);
        }
        progressDialog.show();
    }

    @Override
    public void dismissLoading() {
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    @Override
    public void renderView(ContentDataBean contentDataBean) {
        try{
            if(contentDataBean != null && contentDataBean.getTitle() != null) {
                getSupportActionBar().setTitle(contentDataBean.getTitle());
                if(rowsDataBeen != null && rowsDataBeen.size() > 0) {
                    rowsDataBeen.clear();
                    rowsDataBeen = contentDataBean.getRows();
                    contentDataAdapter.mUpdateData(rowsDataBeen);
                    contentDataAdapter.notifyDataSetChanged();
                } else {
                    rowsDataBeen = contentDataBean.getRows();
                    contentDataAdapter = new ContentDataAdapter(this, rowsDataBeen);
                    mRecyclerView.setAdapter(contentDataAdapter);
                    contentDataAdapter.notifyDataSetChanged();
                }
            }
        }catch(Exception e){
            Log.i(TAG,"Error : " +e.getMessage());
        }
    }
}
