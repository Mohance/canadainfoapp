package com.canadainfoapp.view;

/**
 * Created by Mohan on 28-11-2019.
 */

public interface BaseContract {
    public void alertUser(String title, String text);

    public void displayLoading(String title, String msg);

    public void dismissLoading();
}
