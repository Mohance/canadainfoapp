package com.canadainfoapp.presenter;

import android.util.Log;

import com.canadainfoapp.model.ContentDataLoaderListener;
import com.canadainfoapp.model.ContentDataResponseManager;
import com.canadainfoapp.pojos.ContentDataBean;

/**
 * Created by Mohan on 28-11-2019.
 */

public class ContentDataPresenter implements ContentDataLoaderListener, ContentDataContract.PresenterContract {
    private String TAG = "ContentDataPresenter";
    ContentDataResponseManager contentDataResponseManager;
    ContentDataContract.ViewContract viewContract;

    public ContentDataPresenter(ContentDataContract.ViewContract viewContract) {
        contentDataResponseManager = new ContentDataResponseManager(this);
        this.viewContract = viewContract;
    }

    @Override
    public void onSuccess(ContentDataBean contentList) {
        viewContract.dismissLoading();
        if (contentList != null) {
            Log.i(TAG, "Response : " + contentList.getTitle());
            viewContract.renderView(contentList);
        }
    }

    @Override
    public void onFailure(String error) {
        Log.i(TAG, "Error : " + error);
    }

    public void getContentData() {
        viewContract.displayLoading("Data fetching", "Please wait...");
        contentDataResponseManager.getContentData();
    }
}

