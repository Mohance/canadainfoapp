package com.canadainfoapp.presenter;

import com.canadainfoapp.pojos.ContentDataBean;
import com.canadainfoapp.view.BaseContract;

/**
 * Created by Mohan on 28-11-2019.
 */

public interface ContentDataContract extends BaseContract {
    interface PresenterContract {
        void getContentData();
    }

    interface ViewContract extends BaseContract {
        void renderView(ContentDataBean contentDataBean);
    }
}
