package com.canadainfoapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.canadainfoapp.R;
import com.canadainfoapp.pojos.RowsDataBean;

import java.util.ArrayList;

/**
 * Created by Mohan on 28-11-2019.
 */

public class ContentDataAdapter extends RecyclerView.Adapter<ContentDataAdapter.ViewHolder> {
    private ArrayList<RowsDataBean> mArrayList;
    private Context context;

    public ContentDataAdapter(Context context, ArrayList<RowsDataBean> arrayList) {
        this.context = context;
        mArrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_card_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        if (mArrayList.get(i).getTitle() != null) {
            viewHolder.tvTitle.setText(mArrayList.get(i).getTitle());
            viewHolder.tvDescription.setText(mArrayList.get(i).getDescription());
            if (mArrayList.get(i).getImageHref() != null) {
                Glide.with(context).load(mArrayList.get(i).getImageHref()).into(viewHolder.ivImage);
            } else {
                // Glide doesn't load anything into this view.
                Glide.clear(viewHolder.ivImage);
                // remove the placeholder.
                viewHolder.ivImage.setImageDrawable(null);
            }
        }
    }

    public void mUpdateData(ArrayList<RowsDataBean> arrayList){
        mArrayList = arrayList;
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvDescription;
        private ImageView ivImage;

        public ViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvDescription = (TextView) view.findViewById(R.id.tv_description);
            ivImage = (ImageView) view.findViewById(R.id.iv_image);

        }
    }

}