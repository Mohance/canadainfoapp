package com.canadainfoapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Mohan on 28-11-2019.
 */

public class Utils {
    public static final String BASE_URL = "https://dl.dropboxusercontent.com/";
    public static final String CONTENT_DATA_BEAN = "s/2iodh4vg0eortkl/facts.json";

    //Checks internet connection if available return true or else false.
    public static boolean mChechConnectivity(Context context){
        boolean bStatus = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if(activeNetwork != null){
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI){
                bStatus = true;
            } else if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE){
                bStatus = true;
            }
        }
        return bStatus;
    }
}
