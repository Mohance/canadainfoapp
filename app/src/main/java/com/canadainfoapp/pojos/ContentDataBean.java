package com.canadainfoapp.pojos;

import java.util.ArrayList;

/**
 * Created by Mohan on 28-11-2019.
 */

public class ContentDataBean {
    private String title;

    private ArrayList<RowsDataBean> rows;

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public ArrayList<RowsDataBean> getRows ()
    {
        return rows;
    }

    public void setRows (ArrayList<RowsDataBean> rows)
    {
        this.rows = rows;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [title = "+title+", rows = "+rows+"]";
    }
}

